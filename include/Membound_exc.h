/**
 *       @file  Membound_exc.h
 *      @brief  The Membound BarbequeRTRM application
 *
 * Description: Membound is an application able to create memory noise in the cache
 *  		levels. The application reads and writes to a randomized big chunk of
 *  		memory.
 *
 *     @author  Simone Libutti, simone.libutti@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Simone Libutti
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef MEMBOUND_EXC_H_
#define MEMBOUND_EXC_H_

#include <bbque/bbque_exc.h>
#include <vector>
#include <iostream>
#include <mutex>
#include <sys/prctl.h>

using bbque::rtlib::BbqueEXC;

/**
 * @class Membound
 *
 *  Membound is an application able to create memory noise in the cache
 *  levels. The application reads and writes to a randomized big chunk of
 *  memory.
 */
class Membound : public BbqueEXC {

	// Executions in the single onRun
	int accesses_loops;
	// number of runs
	int cycles;
	// Cache size in bytes
	int cache_size;
	// Cache-line size in bytes
	int bytes_per_line;
	// Associativity
	int ways_number;
	// Intensiveness
	int percent_intensiveness;

	// Equals to cache_size/bytes_per_line
	int lines_per_cache;
	// Equals to lines_per_cache / ways_number
	int lines_per_way;
	//  equals to cache_size / ways_number / bytes_per_line
	int sets_number;

	// Dummy element
	char element='0';

	unsigned char *chunk;
	unsigned char *chunk0;

	// current number of runs
	int performed_cycles;
	// current performed cache accesses
	unsigned long performed_accesses = 0;

	// Indexes storage
	std::vector<int> sets_indexes;

public:

	/**
	 * @brief Constructor
	 */
	Membound(std::string const & name,
			int accesses_loops,
			int cycles,
			int cache_size,
			int bytes_per_line,
			int ways_number,
			int percent_intensiveness,
			std::string const & recipe,
			RTLIB_Services_t *rtlib);

private:

	/**
	 * @brief RTLIB: Initialization
	 */
	RTLIB_ExitCode_t onSetup();
	/**
	 * @brief RTLIB: Configuration, according to the chosen AWM
	 */
	RTLIB_ExitCode_t onConfigure(uint8_t awm_id);
	/**
	 * @brief RTLIB: single execution cycle
	 */
	RTLIB_ExitCode_t onRun();
	/**
	 * @brief RTLIB: monitoring, after a single execution cycle
	 */
	RTLIB_ExitCode_t onMonitor();
	/**
	 * @brief RTLIB: Releasing
	 */
	RTLIB_ExitCode_t onRelease();

};

#endif // MEMBOUND_EXC_H_
