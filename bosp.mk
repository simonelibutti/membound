
ifdef CONFIG_CONTRIB_MEMBOUND

# Targets provided by this project
.PHONY: membound clean_membound

# Add this to the "contrib_testing" target
testing: membound
clean_testing: clean_membound

MODULE_CONTRIB_USER_MEMBOUND=contrib/user/Membound

membound: external
	@echo
	@echo "==== Building Membound ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_USER_MEMBOUND)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_USER_MEMBOUND)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_MEMBOUND)/build/$(BUILD_TYPE) && \
		CXX=$(CXX) CFLAGS="-O0 --sysroot=$(PLATFORM_SYSROOT)" \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_MEMBOUND)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_membound:
	@echo
	@echo "==== Clean-up Membound Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/membound ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/Membound*; \
		rm -f $(BUILD_DIR)/usr/bin/membound*
	@rm -rf $(MODULE_CONTRIB_USER_MEMBOUND)/build
	@echo

else # CONFIG_CONTRIB_MEMBOUND

membound:
	$(warning contib module Membound disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_MEMBOUND
