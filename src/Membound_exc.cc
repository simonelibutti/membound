/**
 *       @file  Membound_exc.h
 *      @brief  The Membound BarbequeRTRM application
 *
 * Description: Membound is an application able to create memory noise in the cache
 *  		levels. The application reads and writes to a randomized big chunk of
 *  		memory.
 *
 *     @author  Simone Libutti, simone.libutti@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Simone Libutti
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#include "Membound_exc.h"

#include <cstdio>
#include <bbque/utils/utility.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <inttypes.h>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <algorithm>

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "aem.membound"
#undef  BBQUE_LOG_UID
#define BBQUE_LOG_UID GetChUid()

Membound::Membound(std::string const & name,
		int accesses_loops,
		int cycles,
		int cache_size,
		int bytes_per_line,
		int ways_number,
		int percent_intensiveness,
		std::string const & recipe,
		RTLIB_Services_t *rtlib) :
	BbqueEXC(name, recipe, rtlib),
	accesses_loops(accesses_loops),
	cycles(cycles),
	cache_size(cache_size),
	bytes_per_line(bytes_per_line),
	ways_number(ways_number),
	percent_intensiveness(percent_intensiveness),
	lines_per_cache(cache_size/bytes_per_line),
	lines_per_way(lines_per_cache/ways_number),
	sets_number(cache_size/(ways_number*bytes_per_line)) {}

RTLIB_ExitCode_t Membound::onSetup() {

	std::srand ( unsigned ( std::time(0) ) );

	// malloc CACHE_SIZE Bytes, aligned to WAY_SIZE Bytes. way_size equals
	// the number of lines in a way * the number of bytes in a line
	chunk = (unsigned char*)aligned_alloc(lines_per_way * bytes_per_line, cache_size);
	memset (chunk, 0, cache_size);

	// saving the index of each set
	for (int i = 0; i < sets_number ; ++i)
		sets_indexes.push_back(i);

	// randomizing the indexes
	std::random_shuffle(sets_indexes.begin(), sets_indexes.end());

	// The percentage need to be in [1-100]
	if (percent_intensiveness > 100)
		percent_intensiveness = 100;

	// I do not need all the sets, but a percentage of them
	int sets = (percent_intensiveness * sets_number) / 100;
	int garbage = sets_number - sets;

	// Popping out the indexes which are not needed
	for (int j = 0; j < garbage ; ++j)
		sets_indexes.pop_back();

	return RTLIB_OK;

}

RTLIB_ExitCode_t Membound::onConfigure(uint8_t awm_id) {

	// No need to configure the application
	return RTLIB_OK;
}

RTLIB_ExitCode_t Membound::onRun() {

	// Performing the same work many times
	for(int i=0; i < accesses_loops; ++i){

		// Each set has to be filled
		for (auto &set : sets_indexes) {

			// Each set contains WAYS_NUMBER lines
			for (int k=0; k < ways_number; ++k) {

				// Fetching the line.
				// Example for SET_0, WAY_SIZE=32K, WAYS_NUMBER 8:
				//	reading BYTES 0, 32k, 64k, ... 224k
				element = chunk[(set * bytes_per_line) +
					(lines_per_way * bytes_per_line * k)];
				++performed_accesses;
			}
		}
	}

	return RTLIB_OK;
}

RTLIB_ExitCode_t Membound::onMonitor() {

	// The number of performed cycles
	performed_cycles = Cycles();

	// If cycles = 0, membound will never stop
	if (performed_cycles == cycles && cycles > 0)
		return RTLIB_EXC_WORKLOAD_NONE;

	return RTLIB_OK;
}

RTLIB_ExitCode_t Membound::onRelease(){

	// Freeing the malloc
	printf("Performed accesses: %lu\n", performed_accesses);
	free (chunk);

	return RTLIB_OK;

}
